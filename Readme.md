# Project for test the Search functionality in https://www.salpo.com
## Technologies used
 - Protractor
- TypeScript
- CSS selectors
- GIT
- IDE- Visual Studio Code

## Instructions to run the script
- clone the source to local folder
- Start up a server with running following command in command prompt:
```sh
webdriver-manager start
```
- navigate to project folder in command prompt
- run following command to run the script
```sh
npm test
```

## Execution Result
view the screenshot of the xecution results by accesing below url
https://www.screencast.com/t/IUTQcVfnzxe
