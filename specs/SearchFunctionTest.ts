import { browser, element, by, $, $$ } from "protractor";

/* Test the functionality of the search function in the salpo site*/
describe('Test the search function in Salpo site', () => {

    beforeEach(() => {
        browser.manage().window().maximize();
        browser.waitForAngularEnabled(false);

        //Access https://www.salpo.com
        browser.get('https://www.salpo.com');
    })

    it('should display the searched options and navigate to correct page', () => {

        //click on the magnifier icon
        $('li#menu-item-search>a').click();

        //enter 'crm' in search text field
        $('input#s').sendKeys('crm');

        //submit the search form
        $('form#searchform').submit();

        //get the search result count and verify
        $('h4.extra-mini-title.widgettitle').getText().then((text) => {
            let resultCount = text.split(" ")[0];

            // Verify that there are more than 10 results
            expect(resultCount).toBeGreaterThan(10);
            console.log('==== '+resultCount + ' results are retieved ====');
        });

        $$('h2.post-title.entry-title>a').then((searchResultElements) => {

            // Verify the count of results in the first page
            expect(searchResultElements.length).toBeGreaterThanOrEqual(10);
            console.log('==== '+searchResultElements.length + ' results are diplaying in the first page ====');

            let resultOneTitle = searchResultElements[0].getText();

            // click on the first result
            searchResultElements[0].click();

            //Verify the selected page is loaded
            expect($("header>h1[itemprop='headline']>a").getText()).toBe(resultOneTitle)
            resultOneTitle.then((resultTitle: string) => {
                console.log('==== '+resultTitle + ' Page is loaded ====');
            });
        });
    });
});