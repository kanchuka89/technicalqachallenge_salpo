"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var HomePage_1 = require("../pages/HomePage");
var NewSearchPage_1 = require("../pages/NewSearchPage");
describe('Test the search function in Salpo site', function () {
    beforeEach(function () {
        protractor_1.browser.manage().window().maximize();
        protractor_1.browser.waitForAngularEnabled(false);
        protractor_1.browser.get('https://www.salpo.com');
    });
    it('should display the searched options and navigate to correct page', function () {
        var homePage = new HomePage_1.HomePage();
        homePage.searchText();
        var newSearchPage = new NewSearchPage_1.NewSearchPage();
        // Verify that there are more than 10 results
        expect(newSearchPage.getResultCount).toBeGreaterThan(10);
        console.log(newSearchPage.getResultCount + ' results are retieved');
        newSearchPage.listSearchResultElements.then(function (searchResultElements) {
            // Verify the count of results in the first page
            expect(searchResultElements.length).toBeGreaterThanOrEqual(10);
            console.log(searchResultElements.length + 'results are diplaying in the first page');
            var resultOneTitle = searchResultElements[0].getText();
            // click on the first result
            searchResultElements[0].click();
            //Verify the selected page is loaded
            expect(protractor_1.$("header>h1[itemprop='headline']>a").getText()).toBe(resultOneTitle);
            resultOneTitle.then(function (resultTitle) {
                console.log(resultTitle + ' Page is loaded');
            });
        });
    });
});
