"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
describe('Enter name feature', function () {
    it('Should ener name as KAnchuka', function () {
        protractor_1.browser.get('https://angularjs.org');
        protractor_1.element(protractor_1.by.model('yourName')).sendKeys('Kanchuka');
        var text = protractor_1.element(protractor_1.by.xpath('/html/body/div[2]/div[1]/div[2]/div[2]/div/h1'));
        expect(text.getText()).toContain('Hello Kanchuka!');
    });
});
