"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
/* Test the functionality of the search function in the salpo site*/
describe('Test the search function in Salpo site', function () {
    beforeEach(function () {
        protractor_1.browser.manage().window().maximize();
        protractor_1.browser.waitForAngularEnabled(false);
        //Access https://www.salpo.com
        protractor_1.browser.get('https://www.salpo.com');
    });
    it('should display the searched options and navigate to correct page', function () {
        //click on the magnifier icon
        protractor_1.$('li#menu-item-search>a').click();
        //enter 'crm' in search text field
        protractor_1.$('input#s').sendKeys('crm');
        //submit the search form
        protractor_1.$('form#searchform').submit();
        //get the search result count and verify
        protractor_1.$('h4.extra-mini-title.widgettitle').getText().then(function (text) {
            var resultCount = text.split(" ")[0];
            // Verify that there are more than 10 results
            expect(resultCount).toBeGreaterThan(10);
            console.log('==== ' + resultCount + ' results are retieved ====');
        });
        protractor_1.$$('h2.post-title.entry-title>a').then(function (searchResultElements) {
            // Verify the count of results in the first page
            expect(searchResultElements.length).toBeGreaterThanOrEqual(10);
            console.log('==== ' + searchResultElements.length + ' results are diplaying in the first page ====');
            var resultOneTitle = searchResultElements[0].getText();
            // click on the first result
            searchResultElements[0].click();
            //Verify the selected page is loaded
            expect(protractor_1.$("header>h1[itemprop='headline']>a").getText()).toBe(resultOneTitle);
            resultOneTitle.then(function (resultTitle) {
                console.log('==== ' + resultTitle + ' Page is loaded ====');
            });
        });
    });
});
