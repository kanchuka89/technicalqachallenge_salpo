"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var NewSearchPage = /** @class */ (function () {
    function NewSearchPage() {
        this.lblResultsCount = protractor_1.$('h4.extra-mini-title.widgettitle');
        this.listSearchResultElements = protractor_1.$$('h2.post-title.entry-title>a');
    }
    NewSearchPage.prototype.getResultCount = function () {
        return this.lblResultsCount.getText().then(function (text) {
            text = text.split(" ")[0];
        });
    };
    return NewSearchPage;
}());
exports.NewSearchPage = NewSearchPage;
