"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var HomePage = /** @class */ (function () {
    function HomePage() {
        this.btnMagnifier = protractor_1.$('li#menu-item-search>a');
        this.txtSearch = protractor_1.$('input#s');
        this.btnSearch = protractor_1.$('form#searchform');
    }
    HomePage.prototype.searchText = function () {
        this.btnMagnifier.click();
        this.txtSearch.sendKeys('crm');
        this.btnSearch.submit();
    };
    return HomePage;
}());
exports.HomePage = HomePage;
